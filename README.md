# deb-norm-opt

Script that edits .deb package so that apt remove won't remove /opt folder
even if it's empty.

### Usage

```
dpkg-deb --build <your deb pack folder>
deb_norm_opt <your deb pack folder> 
```
